#!/bin/bash

ctags-universal -R /home/jonathan/DE4L/*/src/* 
ln -f tags de4l-chaincode-data-offer/tags
ln -f tags de4l-client/tags
ln -f tags de4l-data-delivery-server/tags
ln -f tags de4l-fabric-conf/tags
ln -f tags de4l-payment-server/tags
ln -f tags de4l-pprl/tags
ln -f tags de4l-trading-commons/tags

