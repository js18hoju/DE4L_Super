#!/bin/bash

./createCtags

springBootRun="mvn spring-boot:run"

function print_hl {
	sep='echo "##################################################"'
	echo " "
	echo " "
	eval $sep
	eval $sep
	echo " "
	echo $1
	echo " "
	eval $sep
	eval $sep
	echo " "
}

print_hl "Build trading commons"
cd de4l-trading-commons/
mvn clean install
cd ..


print_hl "start pprl"
cd de4l-pprl/
docker build -t de4l/pprl-do-service --file Dockerfile.do .
xterm -T 'run: PPRL DO-service' -e 'docker run -p 8181:8181 de4l/pprl-do-service' & disown
docker build -t de4l/pprl-linkage-service-h2 --file Dockerfile.lu .
xterm -T 'run:PPRL Linkage-Service' -e 'docker run -p 8185:8185 de4l/pprl-linkage-service-h2' & disown
cd ..


print_hl "start Payment Server"
cd de4l-payment-server/
echo "import certs"
mvn clean
mvn compile exec:java -Dexec.mainClass=scads.de4lpaymentserver.fabric.AddCertsRunner
echo "install"
mvn install
echo "start"
xterm -T 'run:Payment-Server' -e $springBootRun & disown
cd ..


print_hl "start Data Delivery Service"
cd de4l-data-delivery-server/
mvn clean install
xterm -T 'run:Data-Delivery-Service' -e $springBootRun & disown
cd ..

print_hl "Prepare Client"
cd de4l-client/
mvn clean
mvn compile exec:java -Dexec.mainClass="scads.de4lclient.services.AddCertsRunner"



